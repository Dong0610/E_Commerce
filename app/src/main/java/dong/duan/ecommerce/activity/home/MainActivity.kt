package dong.duan.ecommerce.activity.home

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dong.duan.ecommerce.R
import dong.duan.ecommerce.adapter.CardAdapter
import dong.duan.ecommerce.library.base.BaseActivity
import dong.duan.ecommerce.databinding.ActivityMainBinding
import dong.duan.ecommerce.fragment.main.CardFragment
import dong.duan.ecommerce.fragment.main.ExploseFragment
import dong.duan.ecommerce.fragment.main.FragmentAccount
import dong.duan.ecommerce.fragment.main.HomeFrament
import dong.duan.ecommerce.fragment.main.SearchFragment
import dong.duan.ecommerce.fragment.other.FavoriteFragment

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override fun getLayoutResourceId() = R.layout.activity_main

    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater)


    override fun createView() {
        openFragment(HomeFrament(),true)
        binding.bottomNavigation.setOnItemSelectedListener(OnItemSelectedBottomBar)
        binding.edtSearch.setOnClickListener {
            replaceFragment(SearchFragment(), addToBackStack = true)
        }
        binding.icFavorite.setOnClickListener {
            replaceFragment(FavoriteFragment(), addToBackStack = true)
        }
    }

    fun openFragment(fragment: Fragment,value: Boolean= false){
        if(value){
            binding.frameView1.visibility=View.VISIBLE
            binding.frameView2.visibility=View.GONE
            replaceFragment(fragment, R.id.frame_view1, true)
        }
        else{
            binding.frameView1.visibility=View.GONE
            binding.frameView2.visibility=View.VISIBLE
            replaceFragment(fragment, R.id.frame_view2, true)
        }
    }

    private val OnItemSelectedBottomBar =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.iconhome -> {
                    openFragment(HomeFrament(),true)
                    return@OnNavigationItemSelectedListener true
                }

                R.id.ic_search -> {
                    openFragment(ExploseFragment(), true)
                    return@OnNavigationItemSelectedListener true
                }

                R.id.ic_card -> {
                    openFragment(CardFragment())
                    return@OnNavigationItemSelectedListener true
                }

                R.id.ic_discount -> {

                    return@OnNavigationItemSelectedListener true
                }

                R.id.ic_account -> {
                    openFragment(FragmentAccount())
                    return@OnNavigationItemSelectedListener true
                }
            }
            true
        }
}