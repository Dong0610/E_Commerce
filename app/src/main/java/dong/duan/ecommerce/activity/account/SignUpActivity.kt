package dong.duan.ecommerce.activity.account

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import dong.duan.ecommerce.R
import dong.duan.ecommerce.activity.home.MainActivity
import dong.duan.ecommerce.library.base.BaseActivity
import dong.duan.ecommerce.databinding.ActivitySignUpBinding

class SignUpActivity : BaseActivity<ActivitySignUpBinding>() {
    override fun getLayoutResourceId(): Int = R.layout.activity_sign_up
    override fun getViewBinding() = ActivitySignUpBinding.inflate(layoutInflater)
    override fun createView() {
        clickEvent()
        inputEvent()

    }

    private fun inputEvent() {
        binding.edtusname.setOnFocusChangeListener { view, b ->
            txtFocus(b,binding.icUser,binding.llUsname)
        }
        binding.edtemail.setOnFocusChangeListener { view, b ->
            txtFocus(b,binding.icMail,binding.llEmail)
        }
        binding.edtpass.setOnFocusChangeListener { view, b ->
            txtFocus(b,binding.icPasss,binding.llPass)
        }
        binding.edtpassagain.setOnFocusChangeListener { view, b ->
            txtFocus(b,binding.icPasssAgain,binding.llpass2)
        }
            }

    private fun clickEvent() {
        binding.txtSignin.setOnClickListener {
            startActivity(Intent(this@SignUpActivity, SignInActivity::class.java))
        }
        binding.btnSignup.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            }
            startActivity(intent)
        }
    }


}