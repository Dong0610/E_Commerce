package dong.duan.ecommerce.activity.account

import android.content.Intent
import dong.duan.ecommerce.R
import dong.duan.ecommerce.activity.home.MainActivity
import dong.duan.ecommerce.library.base.BaseActivity
import dong.duan.ecommerce.databinding.ActivitySignInBinding

class SignInActivity : BaseActivity<ActivitySignInBinding>() {
    override fun getLayoutResourceId() = R.layout.activity_sign_in
    override fun getViewBinding() = ActivitySignInBinding.inflate(layoutInflater)
    override fun createView() {
        onClick()
        inputEvent()
    }


    private fun inputEvent() {
        binding.edtMail.setOnFocusChangeListener { view, b ->
            txtFocus(b, binding.icMail, binding.llemail)
        }

        binding.edtPass.setOnFocusChangeListener { view, b ->
            txtFocus(b, binding.icPasss, binding.llpass)
        }

        binding.btnSingIn.setOnClickListener {
            var intent = Intent(this, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            }
            startActivity(intent)
        }
    }

    private fun onClick() {
        binding.txtSignup.setOnClickListener {
            startActivity(Intent(this@SignInActivity, SignUpActivity::class.java))
        }
    }


}