package dong.duan.ecommerce.model

import java.io.Serializable

interface IMoldels : Serializable

data class Category(var id: String, val name: String, val icon: Int) : IMoldels
data class Product(
    var id: String, val name: String, var price: Float,
    var saleOff: Float, var idShop: String, var count: Int, var imageUrl: Any,
    var idUser: String, var star: Int, var timeUp: String = "", var isSale: Boolean = true,
    val color: MutableList<ProductColor>? = null,
    val porductSize: MutableList<ProductSize>? = null
) : IMoldels

data class ProductSize(var size: String) : IMoldels
data class ProductColor(var color: String) : IMoldels

data class ProductReview(
    var id: String,
    val productID: String,
    var userImg: Any,
    var useName: String,
    var star: Int,
    var content: String,
    var time: String,
    var listImage: MutableList<Any>? = null
) : IMoldels

data class CardProduct(var islove: Boolean, var nunCount: Int, var product: Product) : IMoldels
data class Order constructor(
    var idorder: String, var time: String, var idUsers: String,
    var count: Int, var status: String, var totalPrice: Float
) : IMoldels

data class Address constructor(
    var idAddress: String, var remindName: String,
    var receiverName:String,
    var location: String, var phoneNunber: String, var phoneNumber2: String=""
) : IMoldels

data class CreditCard constructor(var idCard:String,var imgCard:Any?,
                                  var carName:String,var cardNumber:String,
                                  var securutyCode:String,
                                    var holder:String, var endDate:String,var color:Int):IMoldels

















