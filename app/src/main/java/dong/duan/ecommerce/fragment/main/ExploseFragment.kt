package dong.duan.ecommerce.fragment.main

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.databinding.FragmentExploxeBinding
import dong.duan.ecommerce.databinding.ItemListCategoryBinding
import dong.duan.ecommerce.library.GenericAdapter
import dong.duan.ecommerce.model.Category
import dong.duan.ecommerce.utility.InitData

class ExploseFragment:BaseFragment<FragmentExploxeBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    )=FragmentExploxeBinding.inflate(layoutInflater,container,false)

    var mancategoryAdapter: GenericAdapter<Category, ItemListCategoryBinding>?=null
    var womencategoryAdapter: GenericAdapter<Category, ItemListCategoryBinding>?=null
    override fun initView() {

        mancategoryAdapter= GenericAdapter(InitData.listManStyle, ItemListCategoryBinding::inflate){
                itembinding, category, i ->
            Glide.with(this.requireContext()).load(category.icon).into(itembinding.imgView)
            itembinding.txtName.text=category.name
        }
        womencategoryAdapter= GenericAdapter(InitData.listwomenStyle, ItemListCategoryBinding::inflate){
                itembinding, category, i ->
            Glide.with(this.requireContext()).load(category.icon).into(itembinding.imgView)
            itembinding.txtName.text=category.name
        }
        binding.rcvCategory.adapter=mancategoryAdapter
        binding.rcWomanStyle.adapter=womencategoryAdapter
    }
}