package dong.duan.ecommerce.fragment.other

import android.view.LayoutInflater
import android.view.ViewGroup
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.adapter.ListOrderAdapter
import dong.duan.ecommerce.databinding.FragmentYourOrderBinding
import dong.duan.ecommerce.utility.InitData

class FragmentOrder : BaseFragment<FragmentYourOrderBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentYourOrderBinding.inflate(layoutInflater)

    override fun initView() {
        binding.icBack.setOnClickListener {
            closeFragment(this)
        }
        val adapter= ListOrderAdapter(requireContext())
        binding.rcvListOrder.adapter=adapter
        adapter.setItems(InitData.listOrder)
    }
}