package dong.duan.ecommerce.fragment.main

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.R
import dong.duan.ecommerce.adapter.CardAdapter
import dong.duan.ecommerce.adapter.OnCardEvent
import dong.duan.ecommerce.databinding.FragmentCardBinding
import dong.duan.ecommerce.databinding.FragmentShipToBinding
import dong.duan.ecommerce.dialog.DialogSuccess
import dong.duan.ecommerce.model.CardProduct
import dong.duan.ecommerce.utility.InitData

class CardFragment : BaseFragment<FragmentCardBinding>() {
    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentCardBinding.inflate(layoutInflater)

    override fun initView() {
        initRcv()
        onClick()
    }

    private fun onClick() {
        binding.btnCheckOut.setOnClickListener {
            replaceFullViewFragment(PayAllProduct(120f),addToBackStack = true)
        }
    }

    private fun initRcv() {
        var cardProductAdapter = CardAdapter(object : OnCardEvent {
            override fun onLove(product: CardProduct) {

            }

            override fun onDelete(product: CardProduct) {

            }

            override fun onUpdateCount(price: Float) {

            }
        })

        binding.recyclerView.adapter = cardProductAdapter

        InitData.listProductCard().forEach { cardProduct ->
            Log.d("product", " PRS: ${cardProduct.product.name}")
        }

        cardProductAdapter.setItems(InitData.listProductCard())

    }
}

class PayAllProduct(var sumMoney: Float) : BaseFragment<FragmentShipToBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentShipToBinding.inflate(layoutInflater)

    var Value_Start = 1
    override fun initView() {

        caseView(1)

        binding.btnContinue.setOnClickListener {
            Value_Start += 1
            if (Value_Start > 3) {
                DialogSuccess(this.requireContext(),
                    {
                        this.closeFragment(this)
                    }
                ).show()
            } else {
                caseView(Value_Start)
            }
        }

    }

    fun caseView(value: Int) {
        when (value) {
            1 -> {
                binding.rcvListAdress.visibility = View.VISIBLE
                binding.llChoosePayment.visibility = View.GONE
                binding.rcvListCard.visibility = View.GONE
                binding.icAddAdress.visibility = View.VISIBLE
                binding.btnContinue.text = requireContext().getString(R.string.next)
            }

            2 -> {
                binding.rcvListAdress.visibility = View.GONE
                binding.llChoosePayment.visibility = View.VISIBLE
                binding.rcvListCard.visibility = View.GONE
                binding.icAddAdress.visibility = View.GONE
                binding.btnContinue.text = requireContext().getString(R.string.next)
            }

            3 -> {
                binding.rcvListAdress.visibility = View.GONE
                binding.llChoosePayment.visibility = View.GONE
                binding.rcvListCard.visibility = View.VISIBLE
                binding.icAddAdress.visibility = View.VISIBLE
                binding.btnContinue.text = "$$sumMoney"
            }
        }
    }

}












