package dong.duan.ecommerce.fragment.other

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.R
import dong.duan.ecommerce.databinding.FragmentProductDetailBinding
import dong.duan.ecommerce.databinding.ItemListProductColorBinding
import dong.duan.ecommerce.databinding.ItemListProductSizeBinding
import dong.duan.ecommerce.library.GenericAdapter
import dong.duan.ecommerce.model.Product
import dong.duan.ecommerce.model.ProductColor
import dong.duan.ecommerce.model.ProductSize
import dong.duan.ecommerce.utility.InitData

class ProductFragment(val product: Product) : BaseFragment<FragmentProductDetailBinding>() {
    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentProductDetailBinding.inflate(layoutInflater, container, false)

    override fun initView() {
        initRcv()
        binding.icBack.setOnClickListener {
            closeFragment(this)
        }
        binding.txtSeeMore.setOnClickListener {
            replaceFullViewFragment(ReviewProductFragment(product = product),true)
        }
    }

    var colorAdapter: GenericAdapter<ProductColor, ItemListProductColorBinding>? = null
    var sizeAdapter: GenericAdapter<ProductSize, ItemListProductSizeBinding>? = null
    var currentColor = 0
    var currentSize=0
    @SuppressLint("NotifyDataSetChanged")
    private fun initRcv() {
        colorAdapter = GenericAdapter(
            InitData.listProductColor,
            ItemListProductColorBinding::inflate
        ) { itembinding, item, i ->
            itembinding.root.backgroundTintList=ColorStateList.valueOf(Color.parseColor(item.color))
            itembinding.isSelect.visibility = if(currentColor == i) View.VISIBLE else View.GONE
            itembinding.root.setOnClickListener{
                currentColor=i
                colorAdapter?.notifyDataSetChanged()
            }
        }
        binding.rcvColor.adapter= colorAdapter

        sizeAdapter = GenericAdapter(
            InitData.listProductSize,
            ItemListProductSizeBinding::inflate
        ) { itembinding, item, i ->
            itembinding.txtName.text=item.size
            itembinding.root.setBackgroundResource(
                if(currentSize==i){
                    R.drawable.bg_item_model_category
                }
                else{
                    R.drawable.bg_item_unselect_category
                }
            )
            itembinding.root.setOnClickListener{
                currentSize=i
                sizeAdapter?.notifyDataSetChanged()
            }
        }
        binding.rcvSize.adapter= sizeAdapter
    }

}