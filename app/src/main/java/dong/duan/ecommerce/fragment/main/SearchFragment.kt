package dong.duan.ecommerce.fragment.main

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.R
import dong.duan.ecommerce.activity.home.MainActivity
import dong.duan.ecommerce.adapter.FlashSaleAdapter
import dong.duan.ecommerce.adapter.OnItemSelected
import dong.duan.ecommerce.adapter.OnSearchItem
import dong.duan.ecommerce.adapter.SearchAdapter
import dong.duan.ecommerce.databinding.FragmentSreachBinding
import dong.duan.ecommerce.databinding.ItemViewListCategoryBinding
import dong.duan.ecommerce.library.GenericAdapter
import dong.duan.ecommerce.model.Category
import dong.duan.ecommerce.utility.InitData

class SearchFragment :BaseFragment<FragmentSreachBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    )= FragmentSreachBinding.inflate(layoutInflater,container,false)

     var  adapter:SearchAdapter?=null
    val listCurrent= InitData.listCurrentSeach
    override fun initView() {
        txtFocus(true,
            binding.icSearch,
            binding.llsearch
        )
        initRcv()

        onClick()
        binding.rcvCurrent.adapter=adapter
        adapter?.setItems(listCurrent)
        initListCategory()



    }

    private fun onClick() {
        binding.llChooseCategory.setOnClickListener {
            binding.rcvListCategory.visibility=View.VISIBLE
            binding.rcvResultSearch.visibility=View.GONE
        }


        binding.icClearText.setOnClickListener {
            binding.edtSearchView.setText("")
        }
        binding.icSearch.setOnClickListener {
            binding.rcvCurrent.visibility=View.GONE
            binding.llresult.visibility=View.VISIBLE

        }
        binding.edtSearchView.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p0!!.length==0){
                    adapter?.setItems(listCurrent)
                    binding.icClearText.visibility=View.GONE
                    binding.rcvCurrent.visibility= View.VISIBLE
                    binding.llresult.visibility=View.GONE
                }
                else{
                    binding.icClearText.visibility=View.VISIBLE
                    val listNew= ArrayList<String>()
                    listCurrent.forEach { s->
                        if(s.contains(binding.edtSearchView.text)){
                            listNew.add(s)
                        }
                    }
                    adapter?.setItems(listNew)
                }
            }
            override fun afterTextChanged(p0: Editable?) {
            }
        })

        binding.btnSignup2.setOnClickListener {
            startActivity(Intent(this.context,MainActivity::class.java).apply {
                Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            })
            requireActivity().finish()
        }
    }

    private fun initRcv() {
        val flashSaleAdapter= FlashSaleAdapter(requireContext(), object : OnItemSelected {
            override fun onItemSelect() {

            }

        })
        binding.rcvListCategory.adapter=flashSaleAdapter
        flashSaleAdapter.setItems(InitData.listProduct)

        adapter= SearchAdapter(object :OnSearchItem{
            override fun onSelectItem(value: String) {
                binding.edtSearchView.setText(value)
                binding.rcvCurrent.visibility=View.GONE
                binding.llresult.visibility=View.GONE
            }
        })
    }

    private fun initListCategory() {
        val listAllCategory:MutableList<Category> = mutableListOf()
        listAllCategory.add(0, Category("all","All", R.drawable.ic_all_product))
        InitData.listManStyle.forEach {
            category ->
            listAllCategory.add(category)
        }
        InitData.listwomenStyle.forEach {
                category ->
            listAllCategory.add(category)
        }

        val listCtegory = GenericAdapter(listAllCategory,ItemViewListCategoryBinding::inflate){
            itembiding, category, i ->
            Glide.with(requireContext()).load(category.icon).into(itembiding.imgView)
            itembiding.txtView.text=category.name
            itembiding.root.setOnClickListener{
                binding.nameFilter.text= category.name
                binding.rcvListCategory.visibility=View.GONE
                searchProduct(category.name)
            }
        }
        binding.rcvListCategory.adapter=listCtegory

    }

    private fun searchProduct(name: String) {

    }

    override fun handlerBackPressed() {
        super.handlerBackPressed()
        this.closeFragment(this)
    }

}