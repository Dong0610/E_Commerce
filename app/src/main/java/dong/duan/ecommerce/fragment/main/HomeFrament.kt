package dong.duan.ecommerce.fragment.main

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.R
import dong.duan.ecommerce.adapter.FlashSaleAdapter
import dong.duan.ecommerce.adapter.HomeProductAdapter
import dong.duan.ecommerce.adapter.OnItemSelected
import dong.duan.ecommerce.adapter.OnProductSelected
import dong.duan.ecommerce.adapter.SlideShowAdapter
import dong.duan.ecommerce.databinding.FragmentHomeBinding
import dong.duan.ecommerce.databinding.ItemListCategoryBinding
import dong.duan.ecommerce.fragment.other.FavoriteFragment
import dong.duan.ecommerce.fragment.other.ProductFragment
import dong.duan.ecommerce.fragment.other.SlideBannerFragment
import dong.duan.ecommerce.library.GenericAdapter
import dong.duan.ecommerce.model.Category
import dong.duan.ecommerce.model.Product
import dong.duan.ecommerce.utility.InitData
import java.util.Timer
import kotlin.concurrent.timerTask

class HomeFrament : BaseFragment<FragmentHomeBinding>() {
    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentHomeBinding.inflate(layoutInflater, container, false)

    private var listFragmentBanner = arrayListOf<BaseFragment<*>>()
    private var timer: Timer? = null
    private var timeAutoNextBanner = 5000
    private var handlerNextBanner = Handler(Looper.getMainLooper())

    var categoryAdapter:GenericAdapter<Category,ItemListCategoryBinding>?=null
    override fun initView() {
        initBanner()
       loadRecyview()
    }

    private fun loadRecyview() {
        categoryAdapter=GenericAdapter(InitData.listManStyle,ItemListCategoryBinding::inflate){
                itembinding, category, i ->
            Glide.with(this@HomeFrament.requireContext()).load(category.icon).into(itembinding.imgView)
            itembinding.txtName.text=category.name
        }
        binding.rcvCategory.adapter= categoryAdapter
        val flashSaleAdapter= FlashSaleAdapter(requireContext(), object : OnItemSelected{
            override fun onItemSelect() {

            }
        })

        binding.rcvFlashsale.adapter=flashSaleAdapter
        binding.rcvMegaSale.adapter=flashSaleAdapter
        flashSaleAdapter.setItems(InitData.listProduct)

        val productAdapter= HomeProductAdapter(requireContext(), object : OnProductSelected{
            override fun onItemSelect(product: Product) {
                replaceFullViewFragment(ProductFragment(product),true)
            }

        })
        binding.rcvListProduct.adapter=productAdapter
        productAdapter.setItems(InitData.listProduct)
    }

    private fun initBanner() {
        fun addControl() {
            val adapter = SlideShowAdapter(
                childFragmentManager, listFragmentBanner, requireContext()
            )
            binding.viewPager.adapter = adapter
            binding.dot.attachTo(binding.viewPager)

            binding.viewPager.offscreenPageLimit = 3
        }
        var currentPage=0
        fun autoSlide() {
            binding.viewPager.setCurrentItem(0, true)
            if (timer == null) {
                timer = Timer()
            }
            timer?.schedule(timerTask {
                handlerNextBanner.post {

                    val count = listFragmentBanner.size
                    currentPage = (currentPage + 1) % count
                    if (currentPage == 0) {
                        currentPage = 1
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.viewPager.setCurrentItem(currentPage, false)
                        }, 200)
                    } else {
                        Handler(Looper.getMainLooper()).postDelayed({
                            binding.viewPager.setCurrentItem(currentPage, true)
                        }, 200)
                    }
                }
            }, timeAutoNextBanner.toLong(), timeAutoNextBanner.toLong())
        }
        listFragmentBanner.add(SlideBannerFragment.newInstance(0))
        listFragmentBanner.add(SlideBannerFragment.newInstance(1))
        listFragmentBanner.add(SlideBannerFragment.newInstance(2))
        addControl()
        autoSlide()

    }

}