package dong.duan.ecommerce.fragment.other

import android.view.LayoutInflater
import android.view.ViewGroup
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.databinding.FragmentWriteReviewBinding
import dong.duan.ecommerce.model.Product

class FragmentWriteReview(product: Product):BaseFragment<FragmentWriteReviewBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    )= FragmentWriteReviewBinding.inflate(layoutInflater)

    override fun initView() {
        TODO("Not yet implemented")
    }
}