package dong.duan.ecommerce.fragment.other

import android.view.LayoutInflater
import android.view.ViewGroup
import com.egame.backgrounderaser.aigenerator.base.BaseFragment
import dong.duan.ecommerce.adapter.AllCommentAdapter
import dong.duan.ecommerce.adapter.FilterComment
import dong.duan.ecommerce.adapter.FilterCommentAdapter
import dong.duan.ecommerce.adapter.OnFilterCommentSelect
import dong.duan.ecommerce.databinding.FragmentReviewProductBinding
import dong.duan.ecommerce.model.Product
import dong.duan.ecommerce.utility.InitData

class ReviewProductFragment(product: Product):BaseFragment<FragmentReviewProductBinding>() {
    override fun getBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    )=FragmentReviewProductBinding.inflate(layoutInflater,container,false)

    override fun initView() {
        initRcv()
        binding.icBack.setOnClickListener {
            closeFragment(this@ReviewProductFragment)
        }
    }

    private fun initRcv() {
        binding.rcvColection.adapter=FilterCommentAdapter(object : OnFilterCommentSelect{
            override fun onSelect(filterComment: FilterComment) {

            }
        })

        val allcommentAdapter= AllCommentAdapter(requireContext())
        binding.rcvListComment.adapter=allcommentAdapter
        allcommentAdapter.setItems(InitData.listcomment)

    }
}