package dong.duan.ecommerce.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mobiai.base.basecode.adapter.BaseAdapter
import dong.duan.ecommerce.databinding.ItemListOrderBinding
import dong.duan.ecommerce.model.Order

class ListOrderAdapter(context: Context) : BaseAdapter<Order,ItemListOrderBinding>() {
    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    )= ItemListOrderBinding.inflate(inflater,parent,false)

    override fun bind(binding: ItemListOrderBinding, item: Order, position: Int) {
        binding.txtIdOrder.text=item.idorder
        binding.txtTime.text=item.time
        binding.txtTotalPrice.text="$"+item.totalPrice.toString()
        binding.txtStatus.text=item.status
        binding.txtShipingCount.text= "${item.count} Items purchased"
    }
}