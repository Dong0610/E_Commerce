package dong.duan.ecommerce.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mobiai.base.basecode.adapter.BaseAdapter
import dong.duan.ecommerce.R
import dong.duan.ecommerce.databinding.ItemListColectionCommentBinding
import dong.duan.ecommerce.databinding.ItemListFavoriteViewBinding
import dong.duan.ecommerce.databinding.ItemListImageCommentBinding
import dong.duan.ecommerce.databinding.ItemListReviewProductBinding
import dong.duan.ecommerce.library.GenericAdapter
import dong.duan.ecommerce.model.Product
import dong.duan.ecommerce.model.ProductReview

class AllCommentAdapter(var context: Context):
    BaseAdapter<ProductReview, ItemListReviewProductBinding>(){
    override fun createBinding(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    )= ItemListReviewProductBinding.inflate(inflater,parent,false)

    override fun bind(binding: ItemListReviewProductBinding, item: ProductReview, position: Int) {
        binding.txtName.text=item.useName
        Glide.with(context).load(item.userImg).into(binding.imgUsers)
        binding.rating.rating=item.star.toFloat()
        binding.txtTime.text=item.time
        binding.contentCm.text=item.content
        if(item.listImage!!.size>0){
            binding.listImg.visibility=View.VISIBLE
            val adapter= GenericAdapter(item.listImage!!,ItemListImageCommentBinding::inflate){
                itembinding, s, i ->
                Glide.with(context).load(s).into(itembinding.imagePreview)
            }
            binding.listImg.adapter=adapter
        }
        else{
            binding.listImg.visibility=View.VISIBLE
        }
    }
}
